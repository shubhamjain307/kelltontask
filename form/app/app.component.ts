import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {FormControl} from '@angular/forms';
import {Validators} from '@angular/forms';
import { emp } from './detail';
//import {FormBuilder} from '@angular/cli';
//import {details} from "@angular/cli";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'formlogin';
 //fb:FormBuilder; 
  obj = new emp();
  fg: FormGroup;
  submitted: boolean;
 constructor(private fb:FormBuilder){

 }
  ngOnInit() {
    this.fg = this.fb.group({
      firstname: new FormControl(this.obj.fname, [Validators.required]),
      lastname: new FormControl(this.obj.lname, [Validators.required]),
      gender: new FormControl(this.obj.gender, [Validators.required]),
      contactnum: new FormControl(this.obj.cnt,[Validators.required,Validators.pattern("0-9"),Validators.maxLength(10)]),
      empId:new FormControl(this.obj.eid,[Validators.required,Validators.pattern("0-9"),Validators.maxLength(4)]),
      password:new FormControl(this.obj.pass,[Validators.required,Validators.minLength(8),Validators.maxLength(16)]),
      confirmpassword:new FormControl(this.obj.pass,[Validators.required,Validators.minLength(8),Validators.maxLength(16)])

    })
}

  get fun(){

     return this.fg.controls;
  }
  onsubmit(){
    this.submitted=true;
    if(this.fg.invalid){
      return;
    }

  }

}
